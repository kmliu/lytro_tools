from PIL import Image
import os
from tkFileDialog import askdirectory

print "Choose a folder."
dirname = askdirectory() + "/"
count = -1

for fname in os.listdir(dirname):
    if fname.endswith(".jpeg"):
        filename = fname[:-5]
        im = Image.open(dirname+fname)
        im.save(dirname + filename + ".jpg")
        count += 1
    if fname.endswith(".txt"):
        filename = fname[0:7] + "refocus_depth.txt"
        os.rename(dirname+fname, dirname+filename)

for fname in os.listdir(dirname):
    if fname.endswith("all_focused.jpg"):
        filename = fname[0:7] + "refocus_" + str(count) +".jpg"
        os.rename(dirname+fname, dirname+filename)
